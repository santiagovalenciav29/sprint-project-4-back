const { Orden } = require("../models/pedidos.models");
// const Producto = require("../models/products.models.js");
// const MedioDePago = require("../models/payment.model.js");
// const Usuario = require("../models/usuario.model.js");
const { configure, preferences } = require("mercadopago");
const express = require("express");
const router = express.Router();
const dotenv = require("dotenv");
dotenv.config();

configure({
  access_token: process.env.MERCADOPAGO_TOKEN,
});

const pedidos = [];

const orders = async (req, res) => {
  try {
    const ordenes = await Orden.find({ username: req.body.username });
    if (ordenes) {
      console.log(ordenes[ordenes.length - 1].products);
      const datos = ordenes[ordenes.length - 1].products;
      const productsName = datos.map((x) => x.nombreProducto);
      // console.log("product names :", productsName);
      const quantities = datos.map((x) => x.quantity);
      // console.log(" quantities :", quantities);
      const price = datos.map((x) => x.precio);

      for (let i = 0; i < price.length; i++) {
        pedidos.push({
          title: productsName[i],
          unit_price: price[i],
          quantity: quantities[i],
        });
      }
      res.json(
        "tu orden esta lista para pagarse, dirigete a https://candeleo.ml/pago.html para continuar con el proceso por favor"
      );
    } else {
      res.json(
        "debes crear una orden y agregarle productos para generar el pago"
      );
    }
  } catch (error) {
    console.log(error);
  }
};

/**
 * @swagger
 * /mercadopago/getOrders:
 *  post:
 *      summary: obtener ruta para pagar orden
 *      tags: ['mercadopago']
 *      requestBody:
 *         description: direccion actualizada del usuario logueado
 *         required: false
 *         content:
 *             application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/mercadopago'
 *      security: []
 *      responses:
 *          200:
 *              description: Pedidos obtenidos con exito
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/mercadopago'
 *          401:
 *              description: solo disponible para usuarios logueados
 */
router.post("/getOrders", orders);

router.post("/pago", function (req, res) {
  console.log("New request POST to /pago");
  // TODO: protect this route with a middleware

  // TODO: get user data from the database
  const user = {
    name: "Andrea",
    last_name: "Campanella",
    email: "andrea@campanella.com",
  };

  // TODO: get items from the database
  const amount = req.body.amount;
  let items = [];

  for (let i = 0; i < pedidos.length; i++) {
    items[i] = pedidos[i];
  }
  console.log(items);
  // Crea un objeto de preferencia
  let preference = {
    auto_return: "approved",
    back_urls: {
      success: `${process.env.URL_BACK}/success`, // TODO: define this
      failure: `${process.env.URL_BACK}/failure`, // TODO: define this
      pending: `${process.env.URL_BACK}/pending`, // TODO: define this
    },
    payer: {
      name: user.name,
      surname: user.last_name,
      email: user.email,
    },
    items: items,
  };

  // petición a mercado pago para preparar la compra
  preferences
    .create(preference)
    .then(function (response) {
      // Ok, haga el proceso de pago con este id:
      console.log(response);
      let id = response.body.id;
      res.json({ preference_id: id, url: response.body.sandbox_init_point });
    })
    .catch(function (error) {
      console.log(error);
    });
});

/**
 * @swagger
 * tags:
 *  name: 'mercadopago'
 *  descripcion: en relacion con los pagos de los pedidos de la aplicacion
 * components:
 *  schemas:
 *      mercadopago:
 *          type: object
 *          required:
 *              -username
 *          properties:
 *              username:
 *                  type: string
 *      productResponse:
 *          type: object
 *          required:
 *              - username
 *          properties:
 *              username:
 *                  type: string
 *
 */
module.exports = router;

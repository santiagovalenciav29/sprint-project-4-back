const cors = require("cors");
const passport = require("passport");
const public_routes = require("./routes/public");
const auth_routes = require("./routes/auth");
const payment_routes = require("./routes/payment");
require("./services");

const config = require("./config.js");
const express = require("express");
const morgan = require("morgan");
const app = express();
const PORT = config.module.PORT;
const usuarioRoutes = require("./routes/usuario.route.js");
const swaggerOptions = require("./utils/swaggerO.js");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
require("./db.js");
//const helmet = require('helmet');
const validationToken = require("./middlewares/validationToken.js");

app.use(morgan("dev"));
//app.use(helmet());

app.use(cors());
app.use(passport.initialize());

// parse JSON bodies (as sent by API clients)
app.use(express.json());

app.use(public_routes);
app.use(auth_routes);
app.use(payment_routes);

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use(express.json());

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpecs));
app.use(validationToken.exJwt, validationToken.validationToken);
app.use("/users", usuarioRoutes);
app.use("/mediosDePago", require("./routes/payment.route.js"));
app.use("/productos", require("./routes/product.route.js"));
app.use("/pedidos", require("./routes/pedido.route.js"));
app.use("/cuenta", require("./routes/cuenta.route.js"));

app.listen(PORT, () => {
  console.log("escuchando en el puerto " + PORT);
});

module.exports = app;

//ahora trabajando desde http

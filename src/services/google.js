const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const strategy_name = "google";
const config = require("../config");
// const dotenv = require("dotenv");
// dotenv.config();

const GOOGLE_CLIENT_ID = config.module.GOOGLE_CLIENT_ID;
const GOOGLE_CLIENT_SECRET = config.module.GOOGLE_CLIENT_SECRET;
const GOOGLE_CALLBACK = config.module.GOOGLE_CALLBACK;

passport.use(
  strategy_name,
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: GOOGLE_CALLBACK,
    },
    function (accessToken, refreshToken, profile, done) {
      // User.findOrCreate({ googleId: profile.id }, function (err, user) {
      //   return cb(err, user);
      // });
      return done(null, profile);
    }
  )
);

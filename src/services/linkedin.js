const passport = require("passport");
const linkedinStrategy = require("passport-linkedin-oauth2").Strategy;
const strategy_name = "linkedin";
const config = require("../config");

LINKEDIN_CLIENT_KEY = config.module.LINKEDIN_CLIENT_KEY;
LINKEDIN_CLIENT_SECRET = config.module.LINKEDIN_CLIENT_SECRET;
LINKEDIN_CALLBACK = config.module.LINKEDIN_CALLBACK;

passport.use(
  strategy_name,
  new linkedinStrategy(
    {
      clientID: LINKEDIN_CLIENT_KEY,
      clientSecret: LINKEDIN_CLIENT_SECRET,
      callbackURL: LINKEDIN_CALLBACK,
      scope: ["r_liteprofile", "r_emailaddress"],
    },
    function (accessToken, refreshToken, profile, done) {
      // User.findOrCreate({ googleId: profile.id }, function (err, user) {
      //   return cb(err, user);
      // });
      return done(null, profile);
    }
  )
);

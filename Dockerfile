FROM node:17-alpine

WORKDIR /usr/src/app

COPY . .

RUN npm install

EXPOSE 5000

CMD ["node", "./src/index.js"]